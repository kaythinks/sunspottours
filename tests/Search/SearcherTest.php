<?php
declare(strict_types=1);

namespace Tests\Search;

use MercuryHolidays\Search\Searcher;
use PHPUnit\Framework\TestCase;

class SearcherTest extends TestCase
{
    private $searcher;

    protected function setUp() : void
    {
        $this->searcher = new Searcher();
        $this->searcher->add($this->getDataStub());
    }

    public function testFirstExample() : void
    {
        $response =  $this->searcher->search(2,20.00,30.00);

        $this->assertIsArray($response,"assert variable is array or not");
        $this->assertCount(2,$response, "response array contains the same number of elements");

        $expectedArray = $this->getFirstTestStub();
        $this->assertEquals($expectedArray, $response, " response array is the same as expected array");
    }

    public function testSecondExample() : void
    {
        $response =  $this->searcher->search(2,30.00,50.00);

        $this->assertIsArray($response,"assert variable is array or not");
        $this->assertCount(2,$response, "response array contains the same number of elements");

        $expectedArray = $this->getSecondTestStub();
        $this->assertEquals($expectedArray, $response, " response array is the same as expected array");
    }

    public function testThirdExample() : void
    {   
        $response =  $this->searcher->search(1,25.00,40.00);

        $this->assertIsArray($response,"assert variable is array or not");
        $this->assertCount(3,$response, "response array contains the same number of elements");

        $expectedArray = $this->getThirdTestStub();
        $this->assertEquals($expectedArray, $response, " response array is the same as expected array");
    }

    private function getDataStub() : array
    {
        return [
            [ "name" => "Hotel A ", "available" => false, "floor" => 1,"room_no" => 1,"per_room_price" => 25.80],
            [ "name" => "Hotel A ", "available" => false, "floor" => 1,"room_no" => 2,"per_room_price" => 25.80],
            [ "name" => "Hotel A ", "available" => true, "floor" => 1,"room_no" => 3,"per_room_price" => 25.80],
            [ "name" => "Hotel A ", "available" => true, "floor" => 1,"room_no" => 4,"per_room_price" => 25.80],
            [ "name" => "Hotel A ", "available" => false, "floor" => 1,"room_no" => 5,"per_room_price" => 25.80],
            [ "name" => "Hotel A ", "available" => false, "floor" => 2,"room_no" => 6,"per_room_price" => 30.10],
            [ "name" => "Hotel A ", "available" => true, "floor" => 2,"room_no" => 7,"per_room_price" => 35.00],
            [ "name" => "Hotel B ", "available" => true, "floor" => 1,"room_no" => 1,"per_room_price" => 45.80],
            [ "name" => "Hotel B ", "available" => false, "floor" => 1,"room_no" => 2,"per_room_price" => 45.80],
            [ "name" => "Hotel B ", "available" => true, "floor" => 1,"room_no" => 3,"per_room_price" => 45.80],
            [ "name" => "Hotel B ", "available" => true, "floor" => 1,"room_no" => 4,"per_room_price" => 45.80],
            [ "name" => "Hotel B ", "available" => false, "floor" => 1,"room_no" => 5,"per_room_price" => 45.80],
            [ "name" => "Hotel B ", "available" => false, "floor" => 2,"room_no" => 6,"per_room_price" => 49.00],
            [ "name" => "Hotel B ", "available" => false, "floor" => 2,"room_no" => 7,"per_room_price" => 49.00],
        ];
    }

    private function getFirstTestStub() : array
    {
        return [
            [ "name" => "Hotel A ", "available" => true, "floor" => 1,"room_no" => 3,"per_room_price" => 25.80],
            [ "name" => "Hotel A ", "available" => true, "floor" => 1,"room_no" => 4,"per_room_price" => 25.80],
        ];
    }

    private function getSecondTestStub() : array
    {
        return [
            [ "name" => "Hotel B ", "available" => true, "floor" => 1,"room_no" => 3,"per_room_price" => 45.80],
            [ "name" => "Hotel B ", "available" => true, "floor" => 1,"room_no" => 4,"per_room_price" => 45.80],
        ];
    }

    private function getThirdTestStub() : array
    {
        return [
            [ "name" => "Hotel A ", "available" => true, "floor" => 1,"room_no" => 3,"per_room_price" => 25.80],
            [ "name" => "Hotel A ", "available" => true, "floor" => 1,"room_no" => 4,"per_room_price" => 25.80],
            [ "name" => "Hotel A ", "available" => true, "floor" => 2,"room_no" => 7,"per_room_price" => 35.00]
        ];
    }

    protected function tearDown() : void
    {
        $this->searcher = null;
    }
}
