<?php
declare(strict_types=1);

namespace MercuryHolidays\Search;

use SQLite3;

class Searcher
{
    private $db;

    public function add(array $property): void
    {
        $this->db = new SQLite3(':memory:');
        $this->createTable();

        foreach ($property as $key => $value) {
            $query = "INSERT INTO hotel_details(name,available,floor,room_no,per_room_price)
            VALUES ('{$value['name']}', '{$value['available']}', '{$value['floor']}', '{$value['room_no']}', '{$value['per_room_price']}'); ";

            $this->db->exec($query);
        }
    }

    public function search(int $roomsRequired, float $minimum, float $maximum): array
    {
        $query = ($roomsRequired > 1 ) ? $this->db->query("SELECT name,available,floor,room_no,per_room_price FROM hotel_details WHERE  per_room_price BETWEEN " . $minimum ." AND " . $maximum . " AND  available = TRUE AND room_no > 1
                ORDER BY room_no  ASC LIMIT ". $roomsRequired ) :

            $this->db->query("SELECT name,available,floor,room_no,per_room_price FROM hotel_details WHERE  per_room_price BETWEEN " . $minimum ." AND " . $maximum . " AND  available = TRUE  ORDER BY room_no ASC");

        $data= [];

        while ($result = $query->fetchArray(SQLITE3_ASSOC))
        {
            array_push($data, $result);
        }
        
        return $data;
    }

    private function createTable() : void
    {
         $sql = "CREATE TABLE IF NOT EXISTS hotel_details
        (id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT    NOT NULL,
        available  INTEGER  NOT NULL,
        floor  INTEGER NOT NULL,
        room_no  INTEGER NOT NULL,
        per_room_price  REAL NOT NULL)
        ";

        $this->db->exec($sql);
    }
}

